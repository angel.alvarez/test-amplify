/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateCompanies = /* GraphQL */ `
  subscription OnCreateCompanies {
    onCreateCompanies {
      id
      title
      alttitle
      source_id
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateCompanies = /* GraphQL */ `
  subscription OnUpdateCompanies {
    onUpdateCompanies {
      id
      title
      alttitle
      source_id
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteCompanies = /* GraphQL */ `
  subscription OnDeleteCompanies {
    onDeleteCompanies {
      id
      title
      alttitle
      source_id
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
