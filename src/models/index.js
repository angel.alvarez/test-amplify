// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';



const { Companies } = initSchema(schema);

export {
  Companies
};