import { ModelInit, MutableModel, PersistentModelConstructor } from "@aws-amplify/datastore";





type CompaniesMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

export declare class Companies {
  readonly id: string;
  readonly title?: string;
  readonly alttitle?: string;
  readonly source_id?: string;
  readonly createdAt?: string;
  readonly updatedAt?: string;
  constructor(init: ModelInit<Companies, CompaniesMetaData>);
  static copyOf(source: Companies, mutator: (draft: MutableModel<Companies, CompaniesMetaData>) => MutableModel<Companies, CompaniesMetaData> | void): Companies;
}