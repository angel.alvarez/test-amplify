export type AmplifyDependentResourcesAttributes = {
    "api": {
        "testangel": {
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    }
}